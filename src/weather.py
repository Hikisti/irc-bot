import requests
from config_reader import ConfigReader

class Weather:

    def __init__(self, data=None):
        self.key = ConfigReader().weather_api_key
        self.data = data

    
    def get_data(self, city):
        URL = "http://api.weatherapi.com/v1/current.json"

        params = {
            'key': self.key,
            'q': city
        }

        r = requests.get(url = URL, params = params)
        self.data = r.json()

        if (r.status_code == 200):
            location = self.data['location']['name']
            country = self.data['location']['country']
            temperature_celsius = self.data['current']['temp_c']
            temperature_celsius_feelslike = self.data['current']['feelslike_c']
            condition = self.data['current']['condition']['text']
            wind_kph = self.kph_to_mps(self.data['current']['wind_kph'])
            wind_dir = self.data['current']['wind_dir']

            return "Current weather in {0}, {1}: {2}, {3} °C (feels like {4} °C). Wind: {5} {6} m/s.".format(location, country, condition, temperature_celsius, temperature_celsius_feelslike, wind_dir, wind_kph)
        
        elif (r.status_code == 400):
            return self.handle_errors(r.status_code)

        elif (r.status_code == 401):
            return self.handle_errors(r.status_code)

        elif (r.status_code == 403):
            return self.handle_errors(r.status_code)

        else:
            return "Server error."
        
    def kph_to_mps(self, kph):
        mps = (kph * 1000) / 3600
        return round(mps, 1) 

    def handle_errors(self, status_code):
        error_message = self.data['error']['message']
        if (status_code == 400):
            return "Invalid request: {0}".format(error_message)
        elif (status_code == 401):
            return "Unauthorized: {0}".format(error_message)
        else:
            return "Forbidden: {0}".format(error_message)

if __name__ == '__main__':
    weather = Weather('Hiekkaharju')
    print(weather.get_data())