import json
import requests
from yahoofinancials import YahooFinancials


class YahooFinancialsObject (object):
	
	headers = {
    'User-Agent': ''
	}
	
	def get_stock_price_new(self, ticker):
		if (ticker[0].isupper() == False):
			ticker = ticker.upper()
			
		URL = "https://query1.finance.yahoo.com/v11/finance/quoteSummary/{0}?modules=price".format(ticker)
		r = requests.get(url = URL, headers = YahooFinancialsObject.headers)
		
		# If any of these characters are found from ticker, always return shortName. Otherwise return longName.
		characters_to_check = ['-', '=']
		
		if (r.status_code == 200):
			data = r.json()
			
			try:
				for p in data['quoteSummary']['result']:
					value =  p['price']['regularMarketPrice']['fmt']
					currency = p['price']['currency']
					long_name = p['price']['longName']
					short_name = p['price']['shortName']
					current_percent_change = p['price']['regularMarketChangePercent']['fmt']
					current_change = p['price']['regularMarketChange']['fmt']
					volume = p['price']['regularMarketVolume']['fmt']
					symbol = p['price']['symbol']
					
				if any(x in ticker for x in characters_to_check):
					return "{0} ({1}): {2} {3}, today {4} ({5}). Volume {6}.".format(short_name, symbol, value, currency, current_change, current_percent_change, volume)
				else:
					return "{0} ({1}): {2} {3}, today {4} ({5}). Volume {6}.".format(long_name, symbol, value, currency, current_change, current_percent_change, volume)
				
			except TypeError:
				return "Value not found."
			except UnboundLocalError:
				return "Value not found."
			except KeyError:
				return "Value not found."
				
		elif (r.status_code == 404):
			return "Not found. Try !stock <stock_symbol>."
			
		elif (r.status_code == 503):
			return "Yahoo Finance is down."
			
		else:
			return "Server error."

	def search_stock(self, search_term):
		URL = "https://query2.finance.yahoo.com/v1/finance/search?q={0}".format(search_term)
		r = requests.get(url = URL, headers = YahooFinancialsObject.headers)
		
		if (r.status_code == 200):
			data = r.json()


			list_of_stocks = []
			try:
				for p in data['quotes']:
					symbol = p['symbol']
					short_name = p['shortname']
					exch_disp = p['exchDisp']
					list_of_stocks.append("{0} ({1}) in {2}".format(short_name, symbol, exch_disp))
				return list_of_stocks
					
			except TypeError:
				return "Value not found."
			except UnboundLocalError:
				return "Value not found."
			except KeyError:
				return "Value not found."
				
		elif (r.status_code == 404):
			return "Not found. Try !searchstock <search_string>."
			
		elif (r.status_code == 503):
			return "Yahoo Finance is down."
			
		else:
			return "Server error."
