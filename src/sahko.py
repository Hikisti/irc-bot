import requests
import datetime

class Sahko (object):
	
	headers = {
    'User-Agent': ''
}
	
	def get_current_date(self):
		return datetime.date.today()

	def get_current_hour(self):
		return datetime.datetime.now().hour

	def get_data(self):
		date = self.get_current_date()
		hour = self.get_current_hour()
		URL = "https://api.porssisahko.net/v1/price.json?date={0}&hour={1}".format(date, hour)

		r = requests.get(url = URL, headers=Sahko.headers)
		if (r.status_code == 200):
			data = r.json()
			value = data['price']

			return "{0} {1}".format(value, "snt / kWh")

		elif (r.status_code == 400):
			return "Virheellinen parametri. Hinnan haku epäonnistui."

		elif (r.status_code == 404):
			return "Hintatietoa ei löydy. Pyydetty tunti on liikaa menneisyydessä tai tulevaisuudessa. Hinnan haku epäonnistui."

		else:
			return "Server error."
