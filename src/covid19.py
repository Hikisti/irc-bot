import json
from datetime import date

class Covid19 (object):

	def parse_14_day_cumulative_case_value(self, country):
		if (country[0].isupper() == False):
			country = country.capitalize()
		
		if (country == "England" or country == "Britain"):
			country = "United_Kingdom"

		with open('covid19-data.json') as json_file:
			try:
				data = json.load(json_file)
				for p in data['records']:
					if (p['countriesAndTerritories'] == country):
						cumulative_value = p['notification_rate_per_100000_population_14-days']
						return "14-day cumulative number of COVID-19 cases per 100 000 for {0} is {1:0.1f}".format(country, float(cumulative_value))
						
					elif (p['countriesAndTerritories'].find(country) != -1):
						cumulative_value = p['notification_rate_per_100000_population_14-days']
						return "14-day cumulative number of COVID-19 cases per 100 000 for {0} is {1:0.1f}".format(p['countriesAndTerritories'], float(cumulative_value))
			except json.decoder.JSONDecodeError:
				return "Updating data, please wait a moment and try again."
				
			except KeyError:
				return "Field not found, might be due to API changes."			
