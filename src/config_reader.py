import configparser

class ConfigReader:

    def __init__(self, weather_api_key=None):
        self.weather_api_key = weather_api_key
        self.config = configparser.ConfigParser()
        self.read_config_file()


    def read_config_file(self):
        self.config.read('keys.ini')
        self.weather_api_key = self.config['WeatherAPI']['api_key']

if __name__ == '__main__':
    configreader = ConfigReader()
    configreader.read_config_file()