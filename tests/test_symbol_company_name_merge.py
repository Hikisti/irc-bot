import unittest
import sys
import os

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../src")

from stock_symbols import StockSymbols

class TestSymbolCompanyNameMerge(unittest.TestCase):

	def body_for_tests(self, company_name):
		message = "!osake {}".format(company_name)
		stock_symbols = StockSymbols(message.split(" ", 1)[1])
		stock_symbols.parse_symbols_to_list()
		ticker_symbol = stock_symbols.retrieve_ticker_symbol_by_security_name()
		return ticker_symbol

	def test_google(self):
		ticker_symbol = self.body_for_tests("Google")
		self.assertEqual("GOOGL", ticker_symbol)

	def test_facebook(self):
		ticker_symbol = self.body_for_tests("Facebook")
		self.assertEqual("FB", ticker_symbol)

	def test_3M_Company(self):
		ticker_symbol = self.body_for_tests("3M Company")
		self.assertEqual("MMM", ticker_symbol)

	def test_treehouse(self):
		ticker_symbol = self.body_for_tests("Treehouse")
		self.assertEqual("THS", ticker_symbol)

	def test_tesla(self):
		ticker_symbol = self.body_for_tests("Tesla")
		self.assertEqual("TSLA", ticker_symbol)

	def test_eunl(self):
		ticker_symbol = self.body_for_tests("EUNL")
		self.assertEqual("EUNL", ticker_symbol)

	def test_none(self):
		ticker_symbol = self.body_for_tests("EU NL")
		self.assertEqual(None, ticker_symbol)

if __name__ == "__main__":
    unittest.main()
